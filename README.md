# **prettier** pipeline

<!-- BADGIE TIME -->

[![brettops pipeline](https://img.shields.io/badge/brettops-pipeline-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/brettops/pipelines/prettier?branch=main)](https://gitlab.com/brettops/pipelines/prettier/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/brettops/pipelines/prettier)](https://gitlab.com/brettops/pipelines/prettier/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Format things with [Prettier](https://prettier.io/).

## Usage

Include the pipeline:

```yaml
include:
  - project: brettops/pipelines/prettier
    file:
      - prettier.yml
```
